# HM_Adj_PT

## Hydro-mechanical adjoint pseudo-transient solvers

Copyright (C) 2020  Georg Reuber, Ludovic Raess and Lukas Holbach.

HM_Adj_PT is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HM_Adj_PT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with HM_Adj_PT. If not, see <http://www.gnu.org/licenses/>.

### Current version of HM_Adj_PT can be found at:

https://bitbucket.org/hydromechanics_adjoint_pt/hm_adj_pt/src/master/

http://www.unil.ch/geocomputing/software/

### HM_Adj_PT was released in the Journal of Computational Physics:

```tex
@article{reuber2020_ptadj,
	title = {Adjoint-based inversion for porosity in shallow reservoirs using pseudo-transient solvers for non-linear hydro-mechanical processes},
	journal = {Journal of Computational Physics},
	volume = {423},
	pages = {109797},
	year = {2020},
	issn = {0021-9991},
	doi = {https://doi.org/10.1016/j.jcp.2020.109797},
	url = {https://www.sciencedirect.com/science/article/pii/S0021999120305714},
	author = {Georg S. Reuber and Lukas Holbach and Ludovic Räss},
	keywords = {Two-phase flow, Adjoint gradients, Inversion, Pseudo-transient solver, GPU accelerators}
}
```

### Distributed software, directory content:

[1D_Diffusion_Kernel.ipynb](1D_Diffusion_Kernel.ipynb) iPython notebook to compute the FEM benchmark (FEniCS) of the nonlinear diffusion kernels presented in the Appendix

[1D_Diffusion.ipynb](1D_Diffusion.ipynb) iPython notebook to compute the FEM benchmark (FEniCS) of the nonlinear diffusion example presented in the Appendix

[AdjointSolve.m](AdjointSolve.m) Adjoint PT-FD solver

[ComputeGradientn.m](ComputeGradientn.m) Gradient calculation

[CostFunction.m](CostFunction.m) Cost function evaluation

[ForwardSolve.m](ForwardSolve.m) PT-FD forward solver

[Main.m](Main.m) Adjoint inversion framework main routine

### QUICK START:

MATLAB > select the routine and enjoy!

### Contact: ludovic.rass@gmail.com, reuber@uni-mainz.de
