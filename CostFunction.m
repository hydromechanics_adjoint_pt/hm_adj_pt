function misfit = CostFunction(u,u_obs)
  misfit = 0.5 * sum((u(2:end-1)-u_obs(2:end-1)).^2);
end
