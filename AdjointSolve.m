function [u,evol,res] = AdjointSolve(n,dx,dt,niter,epsi,u_obs,u_forward)
  evol=[];
  u = zeros(size(n));
  i = 1;
  % action
  for iter=1:niter
      Dx         = n.*u_forward.^(n-1);
      Dxa        = (Dx(1:end-1)+Dx(2:end))/2;
      Dx_o       = u_forward.^n;
      Dxa_o      = (Dx_o(1:end-1)+Dx_o(2:end))/2;
      dtau       = 1/(1/(min(dx*dx)/max(Dx)/2.1) + 1/dt);
      te         = diff(u)/dx;
      dudt       = -Dxa(1:end-1).*(diff(u_forward(1:end-1))/dx).*(diff(u(1:end-1))/dx) + u_obs(2:end-1)-u_forward(2:end-1) + diff(Dxa_o.*te)./dx;
      u(2:end-1) = u(2:end-1) + dtau.*dudt;
      u([1,end]) = [1,0.5];   % Boundary conditions
      if mod(iter,100) == 0
          res(i) = max(dudt);
          i = i+1;
      end
      if max(abs(dudt(:))) < epsi, evol=[evol,iter];break; end
  end
end
