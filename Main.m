%% ------------------------------------------------------------------ %%
% Automated inversion for porosity in shallow reservoirs using a       %
% Pseudo-Transient Adjoint solver for the nonlinear hydro-mechanical   %
% equations.                                                           %
%                                                                      %
% by G. Reuber, L. Holbach & L. Räss                                   % 
% Journal of Computational Physics ????                                %
% cite as ???                                                          %
%  ------------------------------------------------------------------  %
% Copyright (C) 2020  Georg Reuber, Ludovic Raess and Lukas Holbach    %
%                                                                      %
% HM_Adj_PT is free software: you can redistribute it and/or modify    %
% it under the terms of the GNU General Public License as published by %
% the Free Software Foundation, either version 3 of the License, or    %
% (at your option) any later version.                                  %
%                                                                      %
% HM_Adj_PT is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of       %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        %
% GNU General Public License for more details.                         %
%                                                                      %
% You should have received a copy of the GNU General Public License    %
% along with HM_Adj_PT. If not, see <http://www.gnu.org/licenses/>.    %
%% ------------------------------------------------------------------ %%

%% Pseudo transient finite difference solver for the 1D nonlinear diffusion equation
% Following equations (41-52) in the main paper

clear
close
% --------- Domain --------- %
Lx    = 20;
nx    = 64;
dx    = Lx/nx;
x     = -Lx/2+dx/2:dx:Lx/2-dx/2;

% --------- PT-FD solver options --------- %
epsi  = 1e-10;
niter = 1e6;

% --------- Initialize --------- %
u     = rand(size(x));
dt    = 100*min(dx*dx)/max(u(:))^3/2.1;
u0    = u;
n = ones(size(u)).*3;   
n_syn = n;

% --------- Calculate synthetic data --------- %
[u_obs,evol,res] = ForwardSolve(u0,n,dx,dt,niter,epsi);
u_syn = u_obs;

% --------- Solve forward initial guess --------- %
n = ones(size(x));    % Homogeneous initial guess
n_ini = n;
[u,evol,res] = ForwardSolve(u0,n,dx,dt,niter,epsi);
u_ini = u;

% --------- Initial cost function --------- %
misfit_ini = CostFunction(u,u_obs);
display(['Initial misfit = ',num2str(misfit_ini)])

% --------- Optimize n  --------- %
tol_rel = 1e-2;
tol_abs = 1e-10;
maxiter = 100;
misfit_track = zeros(maxiter);

iter = 1;
converged = 0;
while iter <  maxiter && converged == 0

    % --------- Solve adjoint equation --------- %
    [u_a,evol_a,res] = AdjointSolve(n,dx,dt,1e5,epsi,u_obs,u);

    % --------- Evaluate gradient --------- %
    gradn = ComputeGradientn(u_a,u,dx,n);
    gradnorm = sqrt(gradn * gradn');

    misfit = CostFunction(u,u_obs);
    misfit_track(iter) = misfit;
    
    if iter == 1
        gradnorm0 = gradnorm;
        alpha     = 1e2;      % initial step length
    end

    % Backtracking linesearch
    it_backtrack = 0;
    backtrack_converged = 0;
    it_backtrack_max = 100;
    n_old = n;
    for it_backtrack = 1:it_backtrack_max
        
        n(1:end-1) = n(1:end-1) - alpha.*gradn;    % update n temporarily
        
        [u,evol,res] = ForwardSolve(u0,n,dx,dt,niter,epsi);     % forward solve with current guess

        misfit_new = CostFunction(u,u_obs);           % check cost function decrease

        % check if backtracking converged
        if misfit_new < misfit
            misfit = misfit_new;
            alpha = alpha * 1.1;
            backtrack_converged = 1;
            break
        else
            n = n_old;   % reset parameters
            alpha = alpha * 0.5;
        end
        
    end
            
    display([num2str(iter),'. misfit = ',num2str(misfit_new),' / relative = ',num2str(misfit_new/misfit_ini)])
            
    if backtrack_converged == 0
        display( 'Backtracking failed. A sufficient descent direction was not found' )
        converged = 0;
        break
    end
    
    if gradnorm < tol_rel*gradnorm0  && iter > 0
        converged = 1;
        misfit_new = CostFunction(u,u_obs); 
        display (['Steepest descent converged in ',num2str(iter),'  iterations'])
    end
        
    iter = iter + 1;
    
    if gradnorm0 < tol_abs
        converged = 1;
        misfit_new = CostFunction(u,u_obs); 
        display ('Steepest descent converged in 1 iteration')
    end
    
end
    
if converged == 0
    display ([ 'Steepest descent did not converge in ', num2str(maxiter), ' iterations'])
end

% Plot final results
figure(1),clf
plot(x(1:end-1),n(1:end-1),'b')
hold on
plot(x(1:end-1),n_syn(1:end-1),'r')
plot(x(1:end-1),n_ini(1:end-1),'g')
ylabel('n'),xlabel('x'),title(['n']) 
legend('n converged','n synthetic','n initial guess')
drawnow

figure(2),clf
plot(x(1:end-1),u(1:end-1),'b')
hold on
plot(x(1:end-1),u_syn(1:end-1),'r')
plot(x(1:end-1),u_ini(1:end-1),'g')
ylabel('u'),xlabel('x'),title(['u']) 
legend('u converged','u synthetic','u initial guess')
drawnow

figure(3),clf
plot(1:length(misfit_track),log10(misfit_track),'b')
ylabel('log10(Cost function)'),xlabel('iterations'),title(['log10(Cost function)']) 
drawnow
