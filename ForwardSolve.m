function [u,evol,res] = ForwardSolve(u,n,dx,dt,niter,epsi)
  evol=[];
  i = 1;
  for iter=1:niter
    Dx         = u.^n;
    Dxa        = (Dx(1:end-1)+Dx(2:end))/2;
    dtau       = 1/(1/(min(dx*dx)/max(Dx)/2.1) + 1/dt);
    qx         = -Dxa.*diff(u)/dx;
    dudt       = -diff(qx)/dx;
    u(2:end-1) = u(2:end-1) + dtau*dudt;
    u([1,end]) = [1,0.5];    % Boundary conditions
    if mod(iter,100) == 0
        res(i) = max(dudt);
        i = i+1;
    end
    if max(abs(dudt(:))) < epsi, evol=[evol,iter];break; end
  end
end
