function gradn = ComputeGradientn(u_a,u,dx,n)
  dudn  = u.^n.*log(u);
  gradn = (diff(u_a)/dx) .* (diff(u)/dx) .* dudn(1:end-1);
end
